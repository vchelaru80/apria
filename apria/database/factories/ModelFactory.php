<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Prescriber::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'npi' => $faker->numberBetween(100000000, 999999999),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'phone' => $faker->numberBetween(100000000, 999999999),
        'phone_ext' => $faker->numberBetween(1000, 9999),
        'fax' => $faker->numberBetween(100000000, 999999999),
        'role' => 'prescriber',
        'is_admin' => $faker->boolean(),
    ];
});
