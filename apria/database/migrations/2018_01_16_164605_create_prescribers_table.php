<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescribersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'prescribers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) {
            return;
        }

        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('npi');
            $table->string('name', 100)->default('');
            $table->string('email', 100)->default('');
            $table->string('password', 100)->default('');
            $table->unsignedInteger('phone');
            $table->unsignedSmallInteger('phone_ext')->nullable()->default(null);
            $table->unsignedInteger('fax')->nullable()->default(null);
            $table->string('role', 50)->default('prescriber');
            $table->unsignedTinyInteger('is_admin')->default('0');
            $table->timestamps();
            //$table->unique('npi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
