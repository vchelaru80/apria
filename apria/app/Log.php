<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['loggable_type', 'loggable_id', 'at', 'by', 'state'];
}
