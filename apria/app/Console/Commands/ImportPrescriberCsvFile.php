<?php

namespace App\Console\Commands;

use App\Prescriber;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ImportPrescriberCsvFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:prescriber {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command imports a prescriber CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file');

        $content = Storage::disk('root')->get($file);

        $collection = collect();

        if (($handle = fopen(base_path() . '/' . $file, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {

                if ($collection->count() > 0) {
                    $collection->push(collect($collection->get(0))->combine($data));
                } else {
                    $collection->push(collect($data));
                }
            }
            fclose($handle);
        }

        Prescriber::saved(function ($prescriber) {
            \App\Log::create([
                'loggable_type' => get_class($prescriber),
                'loggable_id' => $prescriber->id,
                'by' => 'command',
                'at' => (new \DateTime())->getTimestamp(),
                'state' => serialize($prescriber),
            ]);
        });

        $bar = $this->output->createProgressBar(count($collection));

        print "Progress\n\n";

        $inserted = 0;
        $collection->each(function ($item, $key) use (&$inserted, $bar) {
            if ($key > 0) {
                $success = ((new Prescriber())->fill($item->all()))->save();
                if ($success) {
                    $inserted++;
                }

                $bar->advance();
            }
        });

        $bar->finish();

        print "\n\n{$inserted} prescribers are being imported from CSV file import\n";

        $total = Prescriber::count();

        print "{$total} total prescribers were created by the migration and CSV file import\n\n";
    }
}
