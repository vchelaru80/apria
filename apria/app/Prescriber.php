<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescriber extends Model
{
    protected $table = 'prescribers';

    protected $fillable = ['npi', 'name', 'email', 'phone', 'phone_ext', 'fax', 'role', 'is_admin'];

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = !empty($value) ? $value : null;
    }

    public function setPhoneExtAttribute($value)
    {
        $this->attributes['phone_ext'] = !empty($value) ? $value : null;
    }

    public function setFaxAttribute($value)
    {
        $this->attributes['fax'] = !empty($value) ? $value : null;
    }
}
