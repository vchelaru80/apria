<?php

class PrescriberTest extends TestCase
{
    /**
     * Tests if import worked correctly
     *
     * @return void
     */
    public function testImport()
    {
        $total = \App\Prescriber::count();

        $this->assertTrue(($total > 0));
    }

    /**
     * Tests if audit log recoding worked correctly
     *
     * @return void
     */
    public function testLog()
    {
        $total = \App\Log::count();

        $this->assertTrue(($total > 0));
    }
}
