To start docker containers
docker-compose up -d nginx php-fpm redis mysql workspace
 
To login inside laravel container
docker-compose exec --user=laradock workspace bash

To remove all docker images
docker rmi $(docker images -q)

To stop and remove all containers
docker stop $(docker ps -aq) ; docker rm $(docker ps -aq)

run database migration with seeder
php artisan migrate:refresh --seed

Artisan command to import prescribers csv file
php artisan import:prescriber storage/app/public/prescribers.csv 